from django.contrib import admin

# Register your models here.
from .models import Pyrops_wh_Transaction_details

@admin.register(Pyrops_wh_Transaction_details)
class TransactionAdmin(admin.ModelAdmin):
    list_display=['id','item_id','warehouse_id','quantity','expiry_date','mrp','lot_number','transaction_id','transaction_type_value','transaction_ts','request_ts']
